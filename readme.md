# AWS Blog Engine UI
### A React-based front-end component for aws-blog-engine-service

##### Overview
Before deploying this, create a local.config.js file in the `src` folder. This file takes a couple
basic arguments to communicate with the API component:

```javascript
module.exports = {
  apiPath: 'https://example.com/api/',
  basePath: ''
}
```

Only specify a basePath if you want this interface to exist at some other URL than the domain
base URL, i.e. `https://example.com/blog/`.

##### Considerations
You can access the Admin panel at `/admin`. The first user will have to be manually added
in DynamoDB using `createUser.js`.