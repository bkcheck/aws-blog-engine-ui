import Config from '../Config'

function fetchAsJson (path, method, body) {
  return new Promise(function (resolve, reject) {
    getOpts(method, body)
      .then(opts => window.fetch(getPath(path), opts))
      .then(getJson)
      .then(resolve, reject)
  })
}

function getJson (response) {
  return new Promise(function (resolve, reject) {
    response.json()
      .then(json => {
        if (response.ok) {
          resolve(json)
        } else {
          if (response.status === 401) {
            window.location = Config.basePath + '/admin/login'
            resolve()
          } else {
            reject({ message: json.message, status: response.status })
          }
        }
      })
  })
}

function getOpts (method, body) {
  return new Promise(function (resolve) {
    let opts = {
      method,
      credentials: 'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }

    if (body) {
      opts.body = JSON.stringify(body)
    }

    resolve(opts)
  })
}

function getPath (path) {
  return Config.apiPath + path
}

function get (path) {
  return fetchAsJson(path, 'GET')
}

function post (path, body) {
  return fetchAsJson(path, 'POST', body)
}

function put (path, body) {
  return fetchAsJson(path, 'PUT', body)
}

function del (path) {
  return fetchAsJson(path, 'DELETE')
}

const Api = { get, post, put, del }

export default Api
