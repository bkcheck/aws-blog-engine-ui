import React from 'react'
import Api from '../../lib/Api'
import Post from './Post'
import { Link } from 'react-router-dom'
import { parse } from 'querystring'
import propTypes from 'prop-types'
import injectSheet from 'react-jss'

class Posts extends React.Component {
  constructor (props) {
    super(props)
    this.setPostsAndNavLinks = this.setPostsAndNavLinks.bind(this)
    this.state = { posts: [], previous: null, next: null }
  }
  componentDidMount () {
    let url = 'recentPosts'
    let qs = this.props.queryString
    const { start, asc } = parse(qs && qs.substr(1))

    if (start || asc) {
      let queryStringParams = []

      if (start) {
        queryStringParams.push('start=' + start)
      }
      if (asc) {
        queryStringParams.push('asc=' + asc)
      }

      url += '?' + queryStringParams.join('&')
    }

    Api.get(url)
      .then(this.setPostsAndNavLinks)
  }
  render () {
    const { posts, previous, next } = this.state
    const { classes } = this.props
    if (!posts) {
      return null
    }
    return (
      <div>
        {posts.map(post => <Post key={post.slug} post={post} />)}
        {posts.length === 0 &&
          <div>No posts found.</div>
        }
        <div className={classes.navLinks}>
          {previous &&
            <div>
              <Link to={'?start=' + String(previous)}>&lt; Older</Link>
            </div>
          }
          {next &&
            <div className={classes.right}>
              <Link to={'?start=' + String(previous) + '&asc=true'}>Newer &gt;</Link>
            </div>
          }
        </div>
      </div>
    )
  }
  setPostsAndNavLinks (posts) {
    let next, previous

    if (posts && posts.length) {
      let firstPost = posts[posts.length - 1]
      let lastPost = posts[0]

      previous = firstPost.createdOn
      next = lastPost.createdOn
    }

    this.setState({ posts, previous, next })
  }
}

Posts.propTypes = {
  queryString: propTypes.string
}

const css = {
  navLinks: {
    display: 'flex',
    marginBottom: '3em',
    '& > div': {
      width: '50%'
    },
    '& a': {
      color: 'white',
      textDecoration: 'none',
      '&:hover': {
        color: '#BB8833'
      }
    }
  },
  right: {
    textAlign: 'right'
  }
}

export default injectSheet(css)(Posts)
