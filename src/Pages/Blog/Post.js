import React from 'react'
import dateFormat from 'dateformat'
import propTypes from 'prop-types'

class Post extends React.Component {
  render () {
    const { post } = this.props
    return (
      <article id={post.slug}>
        <header className='article-header'>
          <h1>{post.title}</h1>
          <h3>{dateFormat(post.createdOn, 'mmmm dd, yyyy')}</h3>
        </header>
        <div className='article-body' dangerouslySetInnerHTML={{__html: post.content}} />
      </article>
    )
  }
}

Post.propTypes = {
  post: propTypes.shape({
    createdOn: propTypes.string.isRequired,
    slug: propTypes.string.isRequired,
    title: propTypes.string.isRequired,
    content: propTypes.string.isRequired
  })
}

export default Post
