import React from 'react'
import Nav from './Nav'
import Posts from './Posts'

const navStyle = {
  height: '100%',
  position: 'fixed',
  width: '15em'
}

const postStyle = {
  height: '100%',
  marginLeft: '15em',
  overflowY: 'scroll',
  paddingRight: '15em'
}

class Index extends React.Component {
  render () {
    return (
      <div style={{height: '100%'}}>
        <div className='nav' style={navStyle}>
          <Nav />
        </div>
        <div className='posts' style={postStyle}>
          <Posts queryString={this.props.location.search} />
        </div>
      </div>
    )
  }
}

export default Index
