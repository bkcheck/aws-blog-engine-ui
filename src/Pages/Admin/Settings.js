import React from 'react'
import Api from '../../lib/Api'
import Header from '../../Components/Header'
import HeaderButton from '../../Components/HeaderButton'
import Loading from '../../Components/Loading'

class Settings extends React.Component {
  constructor (props) {
    super(props)
    this.state = { settings: '' }
  }
  componentDidMount () {
    Api.get('settings')
      .then(settings => this.setState({ settings }))
  }
  onSave () {

  }
  render () {
    const { settings } = this.state.settings
    return (
      <div>
        <Header title='Settings'>
          <HeaderButton title='Save Settings' onClick={this.onSave} />
        </Header>
        {settings && this.renderForm()}
        {!settings && <Loading fullPage />}
      </div>
    )
  }
}

export default Settings
