import React from 'react'
import Api from '../../../lib/Api'
import Config from '../../../Config'
import Form from '../../../Components/Form'
import Header from '../../../Components/Header'
import HeaderButton from '../../../Components/HeaderButton'
import Loading from '../../../Components/Loading'
import TextInput from '../../../Components/TextInput'
import InjectSheet from 'react-jss'
import AceEditor from 'react-ace'
import propTypes from 'prop-types'
import 'brace/mode/xml'
import 'brace/theme/github'

class Edit extends React.Component {
  constructor (props) {
    super(props)
    this.deletePost = this.deletePost.bind(this)
    this.generateSlug = this.generateSlug.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onContentChange = this.onContentChange.bind(this)
    this.onSave = this.onSave.bind(this)
    this.validatePost = this.validatePost.bind(this)
    this.postId = this.props.match.params.postId
    this.state = { post: '', validationErrors: '' }
  }
  componentWillMount () {
    if (this.postId === 'new') {
      this.setState({ post: { } })
    } else {
      this.loadPost()
    }
  }
  deletePost () {
    if (window.confirm('Are you sure you want to delete this post?')) {
      Api.del('post/' + this.postId)
        .then(this.goUp, err => window.alert(err.message))
    }
  }
  generateSlug (e) {
    e.preventDefault()
    const post = this.state.post
    if (post.title) {
      // https://gist.github.com/mathewbyrne/1280286
      post.slug = post.title.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '') // eslint-disable-line no-useless-escape
        .replace(/\-\-+/g, '-') // eslint-disable-line no-useless-escape
        .replace(/^-+/, '')
        .replace(/-+$/, '')
    }
    this.setState({ post })
  }
  goUp () {
    window.location = Config.basePath + '/admin/posts'
  }
  loadPost () {
    if (this.postId !== 'new') {
      Api.get('post/' + this.postId)
        .then(post => this.setState({ post }))
    } else {
      this.setState({ post: { content: '' } })
    }

    // Should probably beautify/uglify html on save to eliminate whitespace in dynamo
  }
  onChange (name, val) {
    let post = this.state.post
    post[name] = val
    this.setState({ post })
  }
  onContentChange (val) {
    let post = this.state.post
    post.content = val
    this.setState({ post })
  }
  onSave () {
    if (this.validatePost()) {
      Api.put('post', this.state.post)
        .then(this.goUp)
        .catch(err => {
          console.log()
          if (err.status === 409) {
            this.setState({ validationErrors: [err.message] })
          }
        })
    }
  }
  render () {
    const { post } = this.state
    return (
      <div>
        <Header title={this.postId === 'new' ? 'Create Post' : 'Edit Post'}>
          {this.postId !== 'new' &&
            <HeaderButton title='Delete Post' onClick={this.deletePost} type='delete' />
          }
          <HeaderButton title='Save Post' onClick={this.onSave} />
        </Header>
        {post && this.renderForm()}
        {!post && <Loading fullPage />}
      </div>
    )
  }
  renderForm () {
    const c = this.props.classes
    const { post, validationErrors } = this.state
    return (
      <Form validationErrors={validationErrors}>
        <TextInput
          name='title'
          onChange={this.onChange}
          placeholder='Enter post title'
          value={post.title} />
        <div className={c.slugLine}>
          <TextInput
            name='slug'
            onChange={this.onChange}
            placeholder='Enter or generate a post slug'
            value={post.slug} />
          <button onClick={this.generateSlug}>Generate Slug</button>
        </div>
        <AceEditor
          name='content'
          mode='xml'
          onChange={this.onContentChange}
          theme='github'
          width='100%'
          value={post.content} />
      </Form>
    )
  }
  validatePost () {
    let post = this.state.post
    let validationErrors = []

    if (!post.title) {
      validationErrors.push('Post title is required')
    }

    if (!post.slug) {
      validationErrors.push('Post slug is requried. Please enter one or generate it from the title.')
    }

    this.setState({ validationErrors })

    return validationErrors.length === 0
  }
}

Edit.propTypes = {
  classes: propTypes.object,
  match: propTypes.objectOf(propTypes.string)
}

const css = {
  slugLine: {
    display: 'flex',
    '& > button': {
      background: 'none',
      color: '#2495dd',
      cursor: 'pointer',
      height: '1.9em',
      width: '15em'
    },
    '& > input': {
      marginRight: '1em'
    }
  }
}

export default InjectSheet(css)(Edit)
