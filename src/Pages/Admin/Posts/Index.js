import React from 'react'
import { Route } from 'react-router-dom'
import Edit from './Edit'
import List from './List'

class Index extends React.Component {
  render () {
    const { match } = this.props
    return (
      <div style={{ height: '100%' }}>
        <Route exact path={match.url} component={List} />
        <Route path={match.url + '/:postId'} component={Edit} />
      </div>
    )
  }
}

export default Index
