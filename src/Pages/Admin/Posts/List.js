import React from 'react'
import Header from '../../../Components/Header'
import HeaderButton from '../../../Components/HeaderButton'
import List from '../../../Components/List'
import ListItem from '../../../Components/ListItem'
import Loading from '../../../Components/Loading'
import Api from '../../../lib/Api'
import propTypes from 'prop-types'

class PostList extends React.Component {
  constructor (props) {
    super(props)
    this.setData = this.setData.bind(this)
    this.state = { posts: '', previousDate: null, nextDate: null }
  }
  componentWillMount () {
    this.loadPosts()
  }
  loadPosts (date, asc) {
    let url = 'posts'

    Api.get(url)
      .then(this.setData)
  }
  render () {
    const { match } = this.props
    const { posts, previousDate, nextDate } = this.state
    return (
      <div style={{ height: '100%' }}>
        <Header title='Posts'>
          <HeaderButton title='Create Post' href={match.url + '/new'} />
        </Header>
        {posts && posts.length > 0 &&
          <List>
            {posts.map(post => {
              return (
                <ListItem key={post.id}
                  subText={new Date(post.createdOn).toDateString()}
                  title={post.title}
                  href={match.url + '/' + post.id} />
              )
            })}
          </List>
        }
        {posts && posts.length === 0 && 'You have no posts.'}
        {!posts && <Loading fullPage />}
        <div className='footer'>
          {previousDate && this.renderPreviousLink(previousDate)}
          {nextDate && this.renderNextLink(nextDate)}
        </div>
      </div>
    )
  }
  renderNextLink (date) {
    return <a className='next' onClick={() => this.loadPosts(date, true)} />
  }
  renderPreviousLink (date) {
    return <a className='previous' onClick={() => this.loadPosts(date)} />
  }
  setData (posts) {
    let previousDate, nextDate
    if (posts && posts.length) {
      let firstPost = posts[0]
      let lastPost = posts[posts.length - 1]
      previousDate = firstPost.createdOn
      nextDate = lastPost.createdOn
    }
    this.setState({ posts, previousDate, nextDate })
  }
}

PostList.propTypes = {
  match: propTypes.object
}

export default PostList
