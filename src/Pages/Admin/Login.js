import React from 'react'
import injectSheet from 'react-jss'
import Api from '../../lib/Api'
import Config from '../../Config'

class Login extends React.Component {
  constructor (props) {
    super(props)
    this.onSubmit = this.onSubmit.bind(this)
  }
  onSubmit (e) {
    e.preventDefault()
    Api.post('login', { email: e.target.email.value, password: e.target.password.value })
      .then(this.redirect, err => window.alert(err.message))
  }
  redirect () {
    window.location = Config.basePath + '/admin'
  }
  render () {
    const classes = this.props.classes
    return (
      <div className={classes.outer}>
        <div className={classes.inner}>
          <h1 className={classes.logo}>SLSBlog</h1>
          <form onSubmit={this.onSubmit}>
            <input name='email' placeholder='email' />
            <input name='password' placeholder='password' type='password' />
            <button type='submit'>Login</button>
          </form>
        </div>
      </div>
    )
  }
}

const css = {
  inner: {
    height: '15em',
    '& button, input': {
      fontFamily: "'Mukta Mahee', sans-serif",
      fontSize: '1.3em'
    },
    '& button': {
      display: 'block',
      margin: '0 auto',
      padding: '.3em .6em'
    },
    '& input': {
      boxSizing: 'border-box',
      marginBottom: '1em',
      paddingLeft: '.5em',
      width: '100%'
    }
  },
  outer: {
    alignItems: 'center',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    width: '100%'
  },
  logo: {
    color: '#2495dd',
    fontFamily: "'Mukta Mahee', sans-serif",
    fontSize: '2.5em',
    margin: '0 0 .25em',
    textAlign: 'center'
  }
}

export default injectSheet(css)(Login)
