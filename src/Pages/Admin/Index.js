import React from 'react'
import { Route, Link } from 'react-router-dom'
import propTypes from 'prop-types'
import injectSheet from 'react-jss'
import Dashboard from './Dashboard'
import Posts from './Posts/Index'
import Settings from './Settings'
import Config from '../../Config'

class Index extends React.Component {
  constructor (props) {
    super(props)
    this.logout = this.logout.bind(this)
    this.redirect = this.redirect.bind(this)
  }
  logout () {
    document.cookie = 'slsblog=; expires=Thu, 01 Jan 1970 00:00:01 GMT;'
    this.redirect()
  }
  redirect () {
    window.location = Config.basePath + '/admin/login'
  }
  render () {
    const { classes, match } = this.props
    return (
      <div className={classes.wrapper}>
        <nav className={classes.nav}>
          <Link to={match.url}>SLSblog</Link>
          <Link to={match.url + '/posts'}>Posts</Link>
          <Link to={match.url + '/settings'}>Settings</Link>
          <hr />
          <a onClick={this.logout}>Logout</a>
        </nav>
        <main className={classes.main}>
          <Route exact path={match.url} component={Dashboard} />
          <Route path={match.url + '/posts'} component={Posts} />
          <Route path={match.url + '/settings'} component={Settings} />
        </main>
      </div>
    )
  }
}

Index.propTypes = {
  classes: propTypes.object,
  match: propTypes.object
}

const css = {
  main: {
    height: '100%',
    padding: '1em 2em 0',
    width: '100%',
    '& input, button': {
      fontFamily: "'Mukta Mahee', sans-serif",
      fontSize: '1.3em'
    },
    '& input': {
      boxSizing: 'border-box',
      marginBottom: '1em',
      paddingLeft: '.5em',
      width: '100%'
    }
  },
  nav: {
    backgroundColor: '#333',
    color: '#fff',
    fontSize: '1.1em',
    height: '100%',
    paddingTop: '1em',
    width: '12em',
    '& > a': {
      color: '#fff',
      display: 'block',
      margin: '0 0 .5em 1em',
      textDecoration: 'none',
      width: '100%',
      '&:first-of-type': {
        fontSize: '2em',
        margin: '0 0 .5em 0',
        textAlign: 'center'
      },
      '&:hover': {
        color: '#2495dd'
      }
    },
    '& > hr': {
      borderColor: '#999',
      margin: '1em 1.5em 1em'
    }
  },
  wrapper: {
    display: 'flex',
    fontFamily: "'Mukta Mahee', sans-serif",
    height: '100%'
  }
}

export default injectSheet(css)(Index)
