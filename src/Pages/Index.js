import React from 'react'
import { Route } from 'react-router-dom'

import Admin from './Admin/Index'
import Blog from './Blog/Index'
import Login from './Admin/Login'

class Index extends React.Component {
  render () {
    return (
      <div style={{ height: '100%' }}>
        <Route exact path='/admin/login' component={Login} />
        <Route path='/admin' component={Admin} />
        <Route exact path='/' component={Blog} />
      </div>
    )
  }
}

export default Index
