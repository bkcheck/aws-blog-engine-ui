import React from 'react'
import injectSheet from 'react-jss'
import propTypes from 'prop-types'

class List extends React.Component {
  render () {
    const c = this.props.classes
    return (
      <div className={c.list}>
        {this.props.children}
      </div>
    )
  }
}

List.propTypes = {
  children: propTypes.oneOfType([propTypes.array, propTypes.object]),
  classes: propTypes.object
}

const css = {
  list: {
    height: '100%',
    overflowY: 'scroll',
    '& > a:first-of-type': {
      margin: '2em 0 0'
    }
  }
}

export default injectSheet(css)(List)
