import React from 'react'
import { Route } from 'react-router-dom'

import Index from '../Pages/Index'

class App extends React.Component {
  render () {
    return (
      <div style={{height: '100%'}}>
        <Route path='/' component={Index} />
      </div>
    )
  }
}

export default App
