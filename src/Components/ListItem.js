import React from 'react'
import injectSheet from 'react-jss'
import propTypes from 'prop-types'
import { Link } from 'react-router-dom'

class ListItem extends React.Component {
  render () {
    const c = this.props.classes
    let classes = [c.listItem, 'clickable']

    return (
      <Link className={classes.join(' ')} to={this.props.href}>
        <span className={c.title}>{this.props.title}</span>
        {this.props.subText && <span className={c.subText}>{this.props.subText}</span>}
      </Link>
    )
  }
}

ListItem.propTypes = {
  classes: propTypes.object,
  href: propTypes.string,
  subText: propTypes.string,
  title: propTypes.string
}

const css = {
  listItem: {
    borderBottom: '1px solid #eee',
    boxSizing: 'border-box',
    color: '#333',
    display: 'block',
    padding: '1em',
    textDecoration: 'none',
    width: '100%',
    '&.clickable': {
      cursor: 'pointer',
      '&:hover': {
        backgroundColor: '#eee',
        transition: 'background .1s'
      }
    },
    '& > span': {
      display: 'block'
    }
  },
  subText: {
    color: '#555',
    fontSize: '.9em'
  },
  title: {
    fontSize: '1.2em',
    lineHeight: '1.1em'
  }
}

export default injectSheet(css)(ListItem)
