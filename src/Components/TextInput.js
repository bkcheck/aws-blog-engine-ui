import React from 'react'
import propTypes from 'prop-types'

class TextInput extends React.Component {
  constructor (props) {
    super(props)
    this.onChange = this.onChange.bind(this)
  }
  onChange (e) {
    if (this.props.onChange) {
      this.props.onChange(e.target.name, e.target.value)
    }
  }
  render () {
    return (
      <input
        name={this.props.name}
        onChange={this.onChange}
        placeholder={this.props.placeholder}
        value={this.props.value} />
    )
  }
}

TextInput.propTypes = {
  name: propTypes.string.isRequired,
  onChange: propTypes.func,
  placeholder: propTypes.string,
  value: propTypes.any
}

export default TextInput
