import React from 'react'
import propTypes from 'prop-types'

class Loading extends React.Component {
  render () {
    const style = {
      display: 'flex',
      justifyContent: 'center',
      width: '100%'
    }

    if (this.props.fullPage) {
      style.marginTop = '12em'
    }

    return (
      <div style={style}>
        <img src='/rings.svg' alt='Loading...' />
      </div>
    )
  }
}

Loading.propTypes = {
  fullPage: propTypes.bool
}

export default Loading
