import React from 'react'
import { Link } from 'react-router-dom'
import injectSheet from 'react-jss'
import propTypes from 'prop-types'

class HeaderButton extends React.Component {
  constructor (props) {
    super(props)
    this.onClick = this.onClick.bind(this)
  }
  onClick (e) {
    e.preventDefault()
    if (this.props.onClick) {
      this.props.onClick()
    }
  }
  render () {
    const c = this.props.classes
    const classes = [c.action]

    if (this.props.type === 'delete') {
      classes.push('delete')
    }

    if (this.props.onClick) {
      return <a className={classes.join(' ')} onClick={this.onClick}>{this.props.title}</a>
    } else {
      return <Link className={classes.join(' ')} to={this.props.href}>{this.props.title}</Link>
    }
  }
}

HeaderButton.propTypes = {
  classes: propTypes.object,
  href: propTypes.string,
  onClick: propTypes.func,
  title: propTypes.string.isRequired,
  type: propTypes.string
}

const css = {
  action: {
    alignItems: 'center',
    color: '#2495dd',
    cursor: 'pointer',
    display: 'flex',
    height: '100%',
    padding: '0 .75em',
    textDecoration: 'none',
    transition: 'background .1s color .1s',
    '&:hover': {
      backgroundColor: '#ddd'
    },
    '&.delete': {
      color: '#f33',
      '&:hover': {
        backgroundColor: '#fdd'
      }
    }
  }
}

export default injectSheet(css)(HeaderButton)
