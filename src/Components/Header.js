import React from 'react'
import injectSheet from 'react-jss'
import propTypes from 'prop-types'

class Header extends React.Component {
  render () {
    const c = this.props.classes
    return (
      <header className={c.header}>
        <h1>{this.props.title}</h1>
        <div className={c.actions}>
          {this.props.children}
        </div>
      </header>
    )
  }
}

Header.propTypes = {
  classes: propTypes.object.isRequired,
  title: propTypes.string.isRequired,
  children: propTypes.oneOfType([propTypes.array, propTypes.object])
}

const css = {
  actions: {
    display: 'flex',
    float: 'right',
    height: '100%'
  },
  header: {
    borderBottom: '1px #333 solid',
    color: '#333',
    fontWeight: '400',
    height: '3.4em',
    width: '100%',
    '& > h1': {
      display: 'inline-block',
      height: '100%',
      margin: '0'
    }
  }
}

export default injectSheet(css)(Header)
