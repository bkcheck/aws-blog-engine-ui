import React from 'react'
import injectSheet from 'react-jss'
import propTypes from 'prop-types'

class Form extends React.Component {
  render () {
    const c = this.props.classes
    let classes = [c.form]

    if (this.props.validationErrors && this.props.validationErrors.length) {
      classes.push('error')
    }

    return (
      <form className={classes.join(' ')} onSubmit={this.onSubmit}>
        {this.props.validationErrors.length > 0 && this.renderErrors()}
        {this.props.children}
      </form>
    )
  }
  renderErrors () {
    return (
      <div className='validationError'>
        <span>You must correct the following errors before saving:</span>
        <ul>
          {this.props.validationErrors.map(err => <li>{err}</li>)}
        </ul>
      </div>
    )
  }
}

Form.propTypes = {
  children: propTypes.oneOfType([propTypes.array, propTypes.object]),
  classes: propTypes.object,
  onSubmit: propTypes.func,
  validationErrors: propTypes.string
}

const css = {
  form: {
    marginTop: '1em',
    '& > .validationError': {
      backgroundColor: '#fdd',
      fontSize: '1em',
      maxHeight: '0',
      marginBottom: '1em',
      overflow: 'hidden',
      padding: '0 1em',
      transition: 'max-height .5s, padding-top .5s'
    },
    '&.error > .validationError': {
      maxHeight: '10em',
      padding: '1em',
      '& > ul': {
        margin: '0'
      }
    }
  }
}

export default injectSheet(css)(Form)
