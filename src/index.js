import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import './index.css'
import App from './Components/App'
import Config from './Config'
import registerServiceWorker from './registerServiceWorker'

const router = (
  <BrowserRouter basename={Config.basePath}>
    <App />
  </BrowserRouter>
)

ReactDOM.render(router, document.getElementById('main'))
registerServiceWorker()
